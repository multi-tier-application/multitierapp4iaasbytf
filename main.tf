# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }

  required_version = ">= 1.1.0"
  backend "azurerm" {}

}

provider "azurerm" {
  features {}
  client_id       = "ba3e8a34-13fa-4259-80cf-84dfbe63425c"
  client_secret   = var.client_secret
  tenant_id       = "1269359b-e32f-474c-9d79-90e89be50de6"
  subscription_id = "977466de-b86c-4788-b2dc-ecc1c74632ce"
}



resource "azurerm_resource_group" "rg" {
  name     = "three-tier-arch"
  location = var.location-rg
  tags = {
    "Application" = "azure"
  }
}


